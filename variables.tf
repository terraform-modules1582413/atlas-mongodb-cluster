variable "project_prefix" {
  type = string
}
variable "project_environment" {
  type = string
}
variable "project_name" {
  type = string
}

variable "gcp_region" {
  type = string
}

variable "atlas_org_id" {
  type = string
}
variable "atlas_project_id" {
  type = string
}
variable "atlas_cluster_name" {
  type = string
}
variable "atlas_min_instance_size" {
  type    = string
  default = "M10"
}
variable "atlas_autoscaling_enabled" {
  type    = string
  default = false
}
variable "atlas_autoscaling_down_enabled" {
  type    = string
  default = false
}
variable "atlas_max_instance_size" {
  type    = string
  default = "M20"
}
variable "atlas_disk_size_gb" {
  type    = number
  default = 10
}
variable "atlas_disk_autoscaling_enabled" {
  type    = string
  default = false
}
variable "atlas_cloud_backup_enabled" {
  type    = string
  default = false
}
variable "atlas_version_release_system" {
  type    = string
  default = "LTS"
}
variable "atlas_mongo_db_major_version" {
  type    = string
  default = "6.0"
}
variable "atlas_num_shards" {
  type    = number
  default = 1
}
variable "atlas_nodes_size" {
  type    = number
  default = 3
}
variable "atlas_oplog_size_mb" {
  type    = number
  default = 1024
}

variable "atlas_backup_hour" {
  type    = number
  default = 3
}
variable "atlas_backup_minutes" {
  type    = number
  default = 0
}
variable "atlas_backup_daily_enabled" {
  type    = bool
  default = false
}
variable "atlas_backup_daily_retention" {
  type    = number
  default = 1
}
variable "atlas_backup_weekly_enabled" {
  type    = bool
  default = false
}
variable "atlas_backup_weekly_retention" {
  type    = number
  default = 1
}
variable "atlas_backup_monthly_enabled" {
  type    = bool
  default = false
}
variable "atlas_backup_monthly_retention" {
  type    = number
  default = 1
}

variable "atlas_fw_custom" {
  type = list(object({
    name      = string
    ips_cidrs = string
  }))
  default = []
}
