module "name" {
  source = "git::https://gitlab.com/terraform-modules1582413/name.git"

  prefix      = var.project_prefix
  environment = var.project_environment
  name        = var.atlas_cluster_name
  resource    = "atlas-mongodb"
}
