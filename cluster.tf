variable "gcp_regions_mapping" {
  default = {
    "europe-central2" = "EUROPE_CENTRAL_2"
  }
}
locals {
  gcp_region = lookup(var.gcp_regions_mapping, var.gcp_region)
}

resource "mongodbatlas_cluster" "main" {
  project_id = var.atlas_project_id
  name       = module.name.pretty

  cluster_type  = "REPLICASET"
  provider_name = "GCP"

  provider_instance_size_name                     = var.atlas_min_instance_size
  auto_scaling_compute_enabled                    = var.atlas_autoscaling_enabled
  auto_scaling_compute_scale_down_enabled         = var.atlas_autoscaling_down_enabled
  provider_auto_scaling_compute_min_instance_size = var.atlas_min_instance_size
  provider_auto_scaling_compute_max_instance_size = var.atlas_max_instance_size

  disk_size_gb                 = var.atlas_disk_size_gb
  auto_scaling_disk_gb_enabled = var.atlas_disk_autoscaling_enabled

  cloud_backup = var.atlas_cloud_backup_enabled

  version_release_system = var.atlas_version_release_system
  mongo_db_major_version = var.atlas_mongo_db_major_version

  replication_specs {
    num_shards = var.atlas_num_shards
    regions_config {
      region_name     = local.gcp_region
      electable_nodes = var.atlas_nodes_size
      priority        = 7
      read_only_nodes = 0
    }
  }

  advanced_configuration {
    oplog_size_mb = var.atlas_oplog_size_mb
  }

  labels {
    key   = "environment"
    value = var.project_environment
  }

  labels {
    key   = "project"
    value = var.project_name
  }

  labels {
    key   = "name"
    value = var.atlas_cluster_name
  }

  lifecycle { ignore_changes = [provider_instance_size_name] }
}
