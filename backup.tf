resource "mongodbatlas_cloud_backup_schedule" "main" {
  count        = var.atlas_cloud_backup_enabled ? 1 : 0
  project_id   = mongodbatlas_cluster.main.project_id
  cluster_name = mongodbatlas_cluster.main.name

  reference_hour_of_day    = var.atlas_backup_hour
  reference_minute_of_hour = var.atlas_backup_minutes
  restore_window_days      = 1

  dynamic "policy_item_daily" {
    for_each = var.atlas_backup_daily_enabled ? [1] : []
    content {
      frequency_interval = 1
      retention_unit     = "days"
      retention_value    = var.atlas_backup_daily_retention
    }
  }

  dynamic "policy_item_weekly" {
    for_each = var.atlas_backup_weekly_enabled ? [1] : []
    content {
      frequency_interval = 1
      retention_unit     = "weeks"
      retention_value    = var.atlas_backup_weekly_retention
    }
  }

  dynamic "policy_item_monthly" {
    for_each = var.atlas_backup_monthly_enabled ? [1] : []
    content {
      frequency_interval = 1
      retention_unit     = "months"
      retention_value    = var.atlas_backup_monthly_retention
    }
  }
}
