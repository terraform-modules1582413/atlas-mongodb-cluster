<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_mongodbatlas"></a> [mongodbatlas](#requirement\_mongodbatlas) | ~> 1.8.2 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_mongodbatlas"></a> [mongodbatlas](#provider\_mongodbatlas) | ~> 1.8.2 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_name"></a> [name](#module\_name) | git::https://gitlab.com/terraform-modules1582413/name.git | n/a |

## Resources

| Name | Type |
|------|------|
| [mongodbatlas_cloud_backup_schedule.main](https://registry.terraform.io/providers/mongodb/mongodbatlas/latest/docs/resources/cloud_backup_schedule) | resource |
| [mongodbatlas_cluster.main](https://registry.terraform.io/providers/mongodb/mongodbatlas/latest/docs/resources/cluster) | resource |
| [mongodbatlas_project_ip_access_list.apps](https://registry.terraform.io/providers/mongodb/mongodbatlas/latest/docs/resources/project_ip_access_list) | resource |
| [mongodbatlas_project_ip_access_list.default](https://registry.terraform.io/providers/mongodb/mongodbatlas/latest/docs/resources/project_ip_access_list) | resource |
| [mongodbatlas_project_ip_access_list.users](https://registry.terraform.io/providers/mongodb/mongodbatlas/latest/docs/resources/project_ip_access_list) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_atlas_autoscaling_down_enabled"></a> [atlas\_autoscaling\_down\_enabled](#input\_atlas\_autoscaling\_down\_enabled) | n/a | `string` | `false` | no |
| <a name="input_atlas_autoscaling_enabled"></a> [atlas\_autoscaling\_enabled](#input\_atlas\_autoscaling\_enabled) | n/a | `string` | `false` | no |
| <a name="input_atlas_backup_daily_enabled"></a> [atlas\_backup\_daily\_enabled](#input\_atlas\_backup\_daily\_enabled) | n/a | `bool` | `false` | no |
| <a name="input_atlas_backup_daily_retention"></a> [atlas\_backup\_daily\_retention](#input\_atlas\_backup\_daily\_retention) | n/a | `number` | `1` | no |
| <a name="input_atlas_backup_hour"></a> [atlas\_backup\_hour](#input\_atlas\_backup\_hour) | n/a | `number` | `3` | no |
| <a name="input_atlas_backup_minutes"></a> [atlas\_backup\_minutes](#input\_atlas\_backup\_minutes) | n/a | `number` | `0` | no |
| <a name="input_atlas_backup_monthly_enabled"></a> [atlas\_backup\_monthly\_enabled](#input\_atlas\_backup\_monthly\_enabled) | n/a | `bool` | `false` | no |
| <a name="input_atlas_backup_monthly_retention"></a> [atlas\_backup\_monthly\_retention](#input\_atlas\_backup\_monthly\_retention) | n/a | `number` | `1` | no |
| <a name="input_atlas_backup_weekly_enabled"></a> [atlas\_backup\_weekly\_enabled](#input\_atlas\_backup\_weekly\_enabled) | n/a | `bool` | `false` | no |
| <a name="input_atlas_backup_weekly_retention"></a> [atlas\_backup\_weekly\_retention](#input\_atlas\_backup\_weekly\_retention) | n/a | `number` | `1` | no |
| <a name="input_atlas_cloud_backup_enabled"></a> [atlas\_cloud\_backup\_enabled](#input\_atlas\_cloud\_backup\_enabled) | n/a | `string` | `false` | no |
| <a name="input_atlas_cluster_name"></a> [atlas\_cluster\_name](#input\_atlas\_cluster\_name) | n/a | `string` | n/a | yes |
| <a name="input_atlas_disk_autoscaling_enabled"></a> [atlas\_disk\_autoscaling\_enabled](#input\_atlas\_disk\_autoscaling\_enabled) | n/a | `string` | `false` | no |
| <a name="input_atlas_disk_size_gb"></a> [atlas\_disk\_size\_gb](#input\_atlas\_disk\_size\_gb) | n/a | `number` | `10` | no |
| <a name="input_atlas_fw_apps"></a> [atlas\_fw\_apps](#input\_atlas\_fw\_apps) | n/a | <pre>list(object({<br>    name      = string<br>    ips_cidrs = string<br>  }))</pre> | `[]` | no |
| <a name="input_atlas_fw_default"></a> [atlas\_fw\_default](#input\_atlas\_fw\_default) | n/a | <pre>list(object({<br>    name      = string<br>    ips_cidrs = string<br>  }))</pre> | `[]` | no |
| <a name="input_atlas_fw_users"></a> [atlas\_fw\_users](#input\_atlas\_fw\_users) | n/a | <pre>list(object({<br>    name      = string<br>    ips_cidrs = string<br>  }))</pre> | `[]` | no |
| <a name="input_atlas_max_instance_size"></a> [atlas\_max\_instance\_size](#input\_atlas\_max\_instance\_size) | n/a | `string` | `"M20"` | no |
| <a name="input_atlas_min_instance_size"></a> [atlas\_min\_instance\_size](#input\_atlas\_min\_instance\_size) | n/a | `string` | `"M10"` | no |
| <a name="input_atlas_mongo_db_major_version"></a> [atlas\_mongo\_db\_major\_version](#input\_atlas\_mongo\_db\_major\_version) | n/a | `string` | `"6.0"` | no |
| <a name="input_atlas_nodes_size"></a> [atlas\_nodes\_size](#input\_atlas\_nodes\_size) | n/a | `number` | `3` | no |
| <a name="input_atlas_num_shards"></a> [atlas\_num\_shards](#input\_atlas\_num\_shards) | n/a | `number` | `1` | no |
| <a name="input_atlas_oplog_size_mb"></a> [atlas\_oplog\_size\_mb](#input\_atlas\_oplog\_size\_mb) | n/a | `number` | `1024` | no |
| <a name="input_atlas_org_id"></a> [atlas\_org\_id](#input\_atlas\_org\_id) | n/a | `string` | n/a | yes |
| <a name="input_atlas_project_id"></a> [atlas\_project\_id](#input\_atlas\_project\_id) | n/a | `string` | n/a | yes |
| <a name="input_atlas_version_release_system"></a> [atlas\_version\_release\_system](#input\_atlas\_version\_release\_system) | n/a | `string` | `"LTS"` | no |
| <a name="input_gcp_region"></a> [gcp\_region](#input\_gcp\_region) | n/a | `string` | n/a | yes |
| <a name="input_gcp_regions_mapping"></a> [gcp\_regions\_mapping](#input\_gcp\_regions\_mapping) | n/a | `map` | <pre>{<br>  "europe-central2": "EUROPE_CENTRAL_2"<br>}</pre> | no |
| <a name="input_project_environment"></a> [project\_environment](#input\_project\_environment) | n/a | `string` | n/a | yes |
| <a name="input_project_name"></a> [project\_name](#input\_project\_name) | n/a | `string` | n/a | yes |
| <a name="input_project_prefix"></a> [project\_prefix](#input\_project\_prefix) | n/a | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->