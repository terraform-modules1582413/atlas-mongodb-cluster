resource "mongodbatlas_project_ip_access_list" "custom" {
  for_each = { for rule in var.atlas_fw_custom : rule.name => rule }

  project_id = var.atlas_project_id
  cidr_block = each.value.ips_cidrs
  comment    = each.value.name
}
